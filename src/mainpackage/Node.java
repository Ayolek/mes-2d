package mainpackage;

public class Node {
	
	private Point point;
	private int numberOfPoint;
	
	public Node(int numberOfElement, Point point){
		this.numberOfPoint = numberOfElement;
		this.point = point;

	}

	public Point getPoint() {
		return point;
	}
	
	public int getNumberOfPoint() {
		return numberOfPoint;
	}
	
}
