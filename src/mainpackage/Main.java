package mainpackage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.GroupLayout.ParallelGroup;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		
		File file = new File("input.txt");
		Scanner in = new Scanner(file);
		
		Mesh mainMesh = new Mesh(in.nextInt(), in.nextInt(), in.nextDouble(),
								 in.nextDouble(), in.nextDouble(), in.nextDouble());
		
		for(int i=0; i<mainMesh.getNumberOfElements();i++){
			int numberOfElement = in.nextInt();
			mainMesh.elements[numberOfElement] = new Element(numberOfElement, in.nextInt(), in.nextInt(),
											   in.nextInt(), in.nextInt(), in.nextInt(), in.nextInt(), in.nextInt());
		}
		
		for(int i=0; i<mainMesh.getNumberOfNodes();i++){
			int numberOfNode = in.nextInt();
			mainMesh.nodes[numberOfNode] = new Node(numberOfNode, 
								new Point(in.nextInt(), in.nextInt()));
		}		
		
		for(int i=0;i<mainMesh.getNumberOfElements();i++){
			mainMesh.elements[i].calculateJacobian(mainMesh);
			mainMesh.elements[i].calculateInversedJacobian();
			mainMesh.elements[i].calculateLocalMatrix(mainMesh);
			mainMesh.elements[i].calculateLocalVector(mainMesh);
			
		}		
		mainMesh.generateGlobalMatrixAndVector();
		mainMesh.setSystemOfEquations();
		mainMesh.solveSystemOfEquation();
		mainMesh.showTemperatures();
	}
	
}
