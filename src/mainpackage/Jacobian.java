package mainpackage;

public class Jacobian {
	public double[][] jacobian;
	public double detJ;
	
//	public Jacobian(){
//		this.jacobian = new double[2][2];
//		this.jacobian[0][0] = 0.5;
//		this.jacobian[0][1] = 0;
//		this.jacobian[1][0] = 0;
//		this.jacobian[1][1] = 0.5;
//		
//		calculateDetJ();
//	}

	public Jacobian(){
		this.jacobian = new double[2][2];
		this.detJ = 0;
	}
	
	public void calculateDetJ(){
		this.detJ = this.jacobian[0][0]*this.jacobian[1][1]-
				this.jacobian[1][0]*this.jacobian[0][1];
	}
}
