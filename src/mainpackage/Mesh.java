package mainpackage;

import java.util.Arrays;

public class Mesh {

	private int numberOfElements;
	private int numberOfNodes;
	public Element[] elements;
	public Node[] nodes;
	private double ambientTemperature;
	private double alpha;
	private double q;
	private double k;
	public Point localPoints[];
	public Point integrationPoints[];
	public double globalMatrix[][];
	public double globalVector[];
	public double systemOfEquations[][];
	public double temperatures[];
	
	public Mesh(int numberOfElements, int numberOfNodes,
				double ambientTemperature, double alpha,
				double q, double k){
		
		this.numberOfElements = numberOfElements;
		this.numberOfNodes = numberOfNodes;
		this.ambientTemperature = ambientTemperature;
		this.alpha = alpha;
		this.q = q;
		this.k = k;
		
		this.globalMatrix = new double[numberOfNodes][numberOfNodes];
		this.globalVector = new double[numberOfNodes];
		this.temperatures = new double[numberOfNodes];
		this.systemOfEquations = new double[numberOfNodes][numberOfNodes+1];
		this.elements = new Element[this.numberOfElements];
		this.nodes = new Node[this.numberOfNodes];
		this.localPoints = new Point[4];
		this.localPoints[0] = new Point(-1, -1);
		this.localPoints[1] = new Point(1, -1);
		this.localPoints[2] = new Point(1, 1);
		this.localPoints[3] = new Point(-1, 1);
		this.integrationPoints = new Point[4];
		this.integrationPoints[0] = new Point(-0.577, -0.577);
		this.integrationPoints[1] = new Point(0.577, -0.577);
		this.integrationPoints[2] = new Point(0.577, 0.577);
		this.integrationPoints[3] = new Point(-0.577, 0.577);
	}

	public int getNumberOfElements() {
		return numberOfElements;
	}

	public int getNumberOfNodes() {
		return numberOfNodes;
	}

	public double getTemperatureAmbient() {
		return ambientTemperature;
	}

	public double getAlpha() {
		return alpha;
	}

	public double getQ() {
		return q;
	}

	public double getK() {
		return k;
	}

	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}

	public void setNumberOfNodes(int numberOfNodes) {
		this.numberOfNodes = numberOfNodes;
	}

	public void setNodes(Node[] nodes) {
		this.nodes = nodes;
	}

	public void setTemperatureAmbient(double temperatureAmbient) {
		this.ambientTemperature = temperatureAmbient;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public void setQ(double q) {
		this.q = q;
	}

	public void setK(double k) {
		this.k = k;
	}
	
	public double getShapeFunctionDerivative(int numberOfFunction, int derivative, Point p){
		switch(derivative){
		case Derivative.ETA:
			switch(numberOfFunction){
			case 0: return -1.0*(1.0-p.getY())/4;
			case 1: return 1.0*(1.0-p.getY())/4;
			case 2: return 1.0*(1.0+p.getY())/4;
			case 3: return -1.0*(1.0+p.getY())/4;
			default: return -1.0; 
			
		}
		case Derivative.KSI:
			switch(numberOfFunction){
			case 0: return -1.0*(1.0-p.getX())/4;
			case 1: return -1.0*(1.0+p.getX())/4;
			case 2: return 1.0*(1.0+p.getX())/4;
			case 3: return 1.0*(1.0-p.getX())/4;
			default: return -1.0; 
			}		
		}
		return -1.0;
	}
	
	public void setSystemOfEquations(){
		for(int i=0;i<this.getNumberOfNodes()+1;i++)
			for(int j=0;j<this.getNumberOfNodes();j++){
				if(i!=this.getNumberOfNodes())
					this.systemOfEquations[j][i] = globalMatrix[j][i];
				else{
					systemOfEquations[j][i] = globalVector[j];
				}
			}
	}
	
	public void generateGlobalMatrixAndVector(){
		
		for(int i=0; i<this.getNumberOfElements(); i++){
			int n[] = new int[4];
			n[0]=this.elements[i].getNOP(0);
			n[1]=this.elements[i].getNOP(1);
			n[2]=this.elements[i].getNOP(2);
			n[3]=this.elements[i].getNOP(3);
			
			for(int j=0;j<4;j++){
				for(int k=0;k<4;k++){
					globalMatrix[n[j]][n[k]] += elements[i].localMatrix[j][k];
					
				}
				globalVector[n[j]] += elements[i].localVector[j];
			}
		}
	}
	
	public int solveSystemOfEquation(){
		int max;
		double tmp;
		int N = this.getNumberOfNodes();
		double[][] a=systemOfEquations;
		
		for(int i=0; i<N; i++){ // eliminacja
			max=i;
			for(int j=i+1; j<N; j++)
				if(Math.abs(a[j][i])>Math.abs(a[max][i])) max=j; //fabs(x)=|x|, warto�� bezwzgl�dna dla danych double 
			for(int k=i; k<N+1; k++){ // zamiana wierszy warto�ciami
				tmp=a[i][k]; a[i][k]=a[max][k];
				a[max][k]=tmp;
			}
			if(a[i][i]==0)
				return 0;  // Uk�ad sprzeczny!
			for(int j=i+1; j<N; j++)
				for(int k=N; k>=i; k--) // mno�enie wiersza j przez wsp�czynnik "zeruj�cy":
					a[j][k]=a[j][k]-a[i][k]*a[j][i]/a[i][i];
			} // redukcja wsteczna
		
		for(int j=N-1; j>=0; j--){
			tmp=0;
			for(int k=j+1; k<N; k++)
				tmp=tmp+a[j][k]*temperatures[k];
			temperatures[j]=(a[j][N]-tmp)/a[j][j];
			}
		
		return 1;  // wszystko w porz�dku! 
	}

	public void showJacobians(){
		for(int i=0;i<this.getNumberOfElements();i++){
			System.out.println("Jacobian for element:"+i);
			System.out.println(Arrays.toString(this.elements[i].jacobian.jacobian[0]));
			System.out.println(Arrays.toString(this.elements[i].jacobian.jacobian[1]));
			System.out.println("-------------------------------");
		}
	}
	
	public void showJacobiansInversed(){
		for(int i=0;i<this.getNumberOfElements();i++){
			System.out.println("Jacobian for element:"+i);
			System.out.println(Arrays.toString(this.elements[i].inversedJacobian.jacobian[0]));
			System.out.println(Arrays.toString(this.elements[i].inversedJacobian.jacobian[1]));
			System.out.println("-------------------------------");
		}
	}
	
	public void showLocalMatrixes(){
		for(int i=0;i<this.getNumberOfElements();i++){
			System.out.println("matrix for element:"+i);
			System.out.println(Arrays.toString(this.elements[i].localMatrix[0]));
			System.out.println(Arrays.toString(this.elements[i].localMatrix[1]));
			System.out.println(Arrays.toString(this.elements[i].localMatrix[2]));
			System.out.println(Arrays.toString(this.elements[i].localMatrix[3]));
			System.out.println("-------------------------------");
		}
	}
	
	public void showLocalVectors(){
		for(int i=0;i<this.getNumberOfElements();i++){
			System.out.println("Vector for element:"+i);
			System.out.println(Arrays.toString(this.elements[i].localVector));
			System.out.println("-------------------------------");
		}
	}
	
	public void showGlobalVector(){
		System.out.println("Global Vector");
		System.out.println(Arrays.toString(this.globalVector));
	}
	
	public void showGlobalMatrix(){
		System.out.println("Global Matrix:");
		
		for(int i=0;i<this.getNumberOfNodes();i++){			
			System.out.println(Arrays.toString(this.globalMatrix[i]));
		}
	}
	
	public void showTemperatures(){
		System.out.println("Temperatures:");
		
		for(int i=0;i<this.getNumberOfNodes();i++){			
			System.out.println("t"+i+" = "+this.temperatures[i]);
		}
	}
	
}
