package mainpackage;

public class Element {

	private int numberOfElement;
	private int[] NOP;
	private int[] BCNOP;
	private int bc;
	public double[][] localMatrix;
	public double[] localVector;
	public Jacobian jacobian;
	public Jacobian inversedJacobian;
	
	public Element(int numberOfElement, int nop1, int nop2, int nop3, int nop4,
			int bc, int bcnop1, int bcnop2){
		
		this.numberOfElement = numberOfElement;
		this.bc = bc;
		
		this.NOP = new int[4];
		this.NOP[0] = nop1;
		this.NOP[1] = nop2;
		this.NOP[2] = nop3;
		this.NOP[3] = nop4;

		this.BCNOP = new int[2];
		this.BCNOP[0] = bcnop1;
		this.BCNOP[1] = bcnop2;
		
		this.localMatrix = new double[4][4];
		this.localVector = new double[4];
		this.jacobian = new Jacobian();
		this.inversedJacobian = new Jacobian();
	}
	
	public void calculateLocalMatrix(Mesh mesh){
		for(int i=0;i<4;i++){
			for(int j=0;j<4;j++){
				for(int k=0;k<4;k++){
					this.localMatrix[i][j]+=	
					mesh.getK()*this.jacobian.detJ
					*((mesh.getShapeFunctionDerivative(i, Derivative.ETA, mesh.integrationPoints[k])*this.inversedJacobian.jacobian[0][0]
					+mesh.getShapeFunctionDerivative(i, Derivative.KSI, mesh.integrationPoints[k])*this.inversedJacobian.jacobian[0][1])
					*(mesh.getShapeFunctionDerivative(j, Derivative.ETA, mesh.integrationPoints[k])*this.inversedJacobian.jacobian[0][0]
					+mesh.getShapeFunctionDerivative(j, Derivative.KSI, mesh.integrationPoints[k])*this.inversedJacobian.jacobian[0][1])
					+(mesh.getShapeFunctionDerivative(i, Derivative.ETA, mesh.integrationPoints[k])*this.inversedJacobian.jacobian[1][0]
					+mesh.getShapeFunctionDerivative(i, Derivative.KSI, mesh.integrationPoints[k])*this.inversedJacobian.jacobian[1][1])
					*(mesh.getShapeFunctionDerivative(j, Derivative.ETA, mesh.integrationPoints[k])*this.inversedJacobian.jacobian[1][0]
					+mesh.getShapeFunctionDerivative(j, Derivative.KSI, mesh.integrationPoints[k])*this.inversedJacobian.jacobian[1][1]));
				}
			}
		}
		addBoundaryConditions(mesh);
	}
	
	public void addBoundaryConditions(Mesh mesh){
		if(bc == BoundaryConditions.EXCHANGE){
			this.localMatrix[BCNOP[0]][BCNOP[0]]+=mesh.getAlpha()/3;
			this.localMatrix[BCNOP[0]][BCNOP[1]]+=mesh.getAlpha()/6;
			this.localMatrix[BCNOP[1]][BCNOP[0]]+=mesh.getAlpha()/6;
			this.localMatrix[BCNOP[1]][BCNOP[1]]+=mesh.getAlpha()/3;
		}
	}
	
	public void calculateLocalVector(Mesh mesh){
		switch(this.bc){
		case BoundaryConditions.EXCHANGE:
			localVector[BCNOP[0]]= mesh.getAlpha()*mesh.getTemperatureAmbient()/2;
			localVector[BCNOP[1]]= mesh.getAlpha()*mesh.getTemperatureAmbient()/2;
			break;
		case BoundaryConditions.FLUX:
			localVector[BCNOP[0]]= 0.5*mesh.getQ();
			localVector[BCNOP[1]]= 0.5*mesh.getQ();
		}
	}
	
	public void calculateJacobian(Mesh mesh){
		for(int k=0;k<4;k++){
			this.jacobian.jacobian[0][0] +=
					mesh.getShapeFunctionDerivative(k, Derivative.ETA, mesh.localPoints[3])
					*mesh.nodes[NOP[k]].getPoint().getX();
			this.jacobian.jacobian[0][1] +=
					mesh.getShapeFunctionDerivative(k, Derivative.ETA, mesh.localPoints[2])
					*mesh.nodes[NOP[k]].getPoint().getY();
			this.jacobian.jacobian[1][0] +=
					mesh.getShapeFunctionDerivative(k, Derivative.KSI, mesh.localPoints[1])
					*mesh.nodes[NOP[k]].getPoint().getX();
			this.jacobian.jacobian[1][1] +=
					mesh.getShapeFunctionDerivative(k, Derivative.KSI, mesh.localPoints[0])
					*mesh.nodes[NOP[k]].getPoint().getY();
		}
		this.jacobian.calculateDetJ();	
	}
	
	public void calculateInversedJacobian(){
		this.inversedJacobian.jacobian[0][0] = this.jacobian.jacobian[1][1]/this.jacobian.detJ;
		this.inversedJacobian.jacobian[0][1] = this.jacobian.jacobian[1][0]*-1/this.jacobian.detJ;
		this.inversedJacobian.jacobian[1][0] = this.jacobian.jacobian[0][1]*-1/this.jacobian.detJ;;
		this.inversedJacobian.jacobian[1][1] = this.jacobian.jacobian[0][0]/this.jacobian.detJ;
		
		this.inversedJacobian.calculateDetJ();
	}
	
	public int getNOP(int i){
		return NOP[i];
	}
}
